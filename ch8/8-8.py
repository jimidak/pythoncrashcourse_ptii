# 8-8. 
# User Albums: 
# Start with your program from Exercise 8-7. 
# Write a while loop that allows users to enter an album’s artist and title. 
# Once you have that information, call make_album() with the user’s input and print the dictionary that’s created. 
# Be sure to include a quit value in the while loop.
#
def make_album(n, t, s=None):
    album={"Artist_Name":n, "Album":t}
    if s:
        album['Song_Count'] = s
    return album

catalog_max=0
## Add 5 Albums ##
while catalog_max < 6:
    album = input("What is the name of the album? ")
    artist = input("What is the name of the band or artist? ")
    numsongs = input("How many songs are on the album? ")
    print(make_album(album.title(), artist.title(), int(numsongs)))
    catalog_max += 1