# 8-10.       Sending Messages: (copy of your program from Exercise 8-9) 
# Write a function called send_messages() 
# that prints each text message and moves each message to a new list called sent_messages as it’s printed. 
# After calling the function, print both of your lists to make sure the messages were moved correctly.
#
texts=[
    "Word to your Mother", 
    "David likes llamas", 
    "Corey makes burgers for Snoopy", 
    "To infinity, and Beyond!"
    ]

sent_texts=[]

def send_messages(working_lst, completed_lst):
    '''A function to processes items from the first list to the second list'''
    while working_lst:
        completed_lst.append(working_lst.pop())
        print(completed_lst[-1])
send_messages(texts, sent_texts)
print(f"\nChecking to see what is in 'texts' and 'sent_texts' lists, after running function\n")
print(texts)
print(sent_texts)