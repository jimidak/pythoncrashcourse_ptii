# 8-9. 
# Messages: 
# Make a list containing a series of short text messages. 
# Pass the list to a function called show_messages(), which prints each text message.
#
texts=[
    "Word to your Mother", 
    "David likes llamas", 
    "Corey makes burgers for Snoopy", 
    "To infinity, and Beyond!"
    ]

def show_messages(lst):
    for x in lst:
        print(x)

show_messages(texts)