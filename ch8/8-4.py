#8-4. 
# Large Shirts: 
# Modify the make_shirt() function so that shirts are large
# by default with a message that reads I love Python. 
# Make a large shirt and a medium shirt with the default message, and a shirt of any size with a different message.
#
def make_shirt(size="large", msg="I love Python"):
    print(f"Your shirt is a {size.upper()}, and will have '{msg}' printed on it")

make_shirt("large")
make_shirt("meduim")
make_shirt("small", "Snoop Dogg eats Buffalo Burgers")
make_shirt()