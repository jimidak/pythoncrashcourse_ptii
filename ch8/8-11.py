# 8-11. 
# Archived Messages: 
# Start with your work from Exercise 8-10. 
# Call the function send_messages() with a copy of the list of messages. 
# After calling the function, print both of your lists to show that the original list has retained its messages.
#
texts=[
    "Word to your Mother", 
    "David likes llamas", 
    "Corey makes burgers for Snoopy", 
    "To infinity, and Beyond!"
    ]
messages=texts[:]

sent_texts=[]

def send_messages(working_lst, completed_lst):
    '''A function to processes items from the first list to the second list'''
    while working_lst:
        completed_lst.append(working_lst.pop())
        print(completed_lst[-1])

send_messages(messages, sent_texts)
print(f"\nChecking to see what is in 'texts' and 'sent_texts' lists, after running function\n")
print(texts)
print(sent_texts)