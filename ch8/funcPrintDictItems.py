#########################
## Messing w functions ##
#########################
##
'''
Ask David, Corey, Rich and Alison what thier favorite fruit is.
In your text editor, create a function with two arguments.
When that function with arguments is called, 
it will print a sentence that includes a person's fist name and the name of the fruit".
'''
fruitdic={"david":"nectarine", "corey":"huckelberry", "rich":"peach", "alison":"blueberries"}

def fruit():
    for k, v in fruitdic.items():
        print(f"{k}'s favorite fruit is {v}")

fruit()