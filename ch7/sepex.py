#SEP examples
list1=["snoopy", "zach", "nilla", "joey"]

print(f"this prints the list as-is")
print(list1)
print("\n")

print(f"this prints the list without brackets")
print(*list1)
print("\n")

print(f"this prints the list without brackets, comma seperaterd")
print(*list1, sep=", ")
print("\n")