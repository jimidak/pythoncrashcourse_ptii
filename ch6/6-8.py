#6-8. 
#Pets:
''' 
Make several dictionaries, where each dictionary represents a different pet. 
In each dictionary, include the kind of animal and the owner’s name. 
Store these dictionaries in a list called pets. 
Next, loop through your list and as you do, print everything you know about each pet.
'''
richpet={"pname":"nilla", "animal":"cat", "breed":"domestic short hair", "age    ":str(5), "oname":"rj"}
davepet={"pname":"zach", "animal":"dog", "breed":"shitzu", "age    ":str(12), "oname":"heather"}
corypet={"pname":"rover", "animal":"dog", "breed":"pug", "age    ":str(18), "oname":"corey"}
pets=[richpet, davepet, corypet]
print("MY FRIEND'S PETS")
for i in pets:
    print("#"*15)
    for k, v in i.items():
        print(f"{k}\t {v}")
