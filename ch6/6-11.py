'''
6-11. 
Cities: 
Make a dictionary called cities. Use the names of three cities as keys in your dictionary. 
Create a dictionary of information about each city and include the country that the city is in, 
its approximate population, and one fact about that city. 
The keys for each city’s dictionary should be something like country, population, and fact. 
Print the name of each city and all of the information you have stored about it.
'''
cities={
    "austin":{"population":"1M", "country":"usa", "fun-fact":"home of Willie Nelson"}, 
    "los angles":{"population":"4M", "country":"usa", "fun-fact":"has tar pits"}, 
    "toronto":{"population":"3M", "country":"canada", "fun-fact":"the Raptors won the NBA title"}
    }
for k, v in cities.items():
    print(f"*{k.upper()}* ")
    for kk, vv in v.items():
        print(f"{kk} \t{vv.upper()}")
        #print(f"\t{i[1]}")
    print(f"\n")