#6-10. 
#Favorite Numbers: 
#Modify your program from Exercise 6-2 (page 99) so each person can have more than one favorite number. 
#Then print each person’s name along with their favorite numbers.
'''
6-2
import json
#6-2. Favorite Numbers: 
Use a dictionary to store people’s favorite numbers. 
Think of five names, and use them as keys in your dictionary. 
Think of a favorite number for each person, and store each as a value in your dictionary. 
Print each person’s name and their favorite number. 
For even more fun, poll a few friends and get some actual data for your program.
'''
#########
d={
    "dave ":[42, 6, 12], 
    "rj   ":[42, 25], 
    "corey":[1, 67], 
    "rich ":[16, 18, 7], 
    "brad ":[25, 1]
    }
#########
print("FAVORITE NUMBERS")
print("----------------")
for k, v in d.items():
    print(k +"\t", *v, sep=": " )