#6-9. 
#Favorite Places: 
'''
Make a dictionary called favorite_places. Think of three names to use as keys in the dictionary, 
and store one to three favorite places for each person. 
To make this exercise a bit more interesting, ask some friends to name a few of their favorite places. 
Loop through the dictionary, and print each person’s name and their favorite places.
'''
favorite_places={
    "rich":['vermillion', 'boulder', 'new york city'], 
    "david":['snow mountain ranch', 'round bald sheep'], 
    "rj":['london', 'golden']
    }
for k, v in favorite_places.items():
    print(f"{k.title()}\n////////////////////////")
    for place in v:
        if place == "new york city":
            print(f"\t- NYC")
        else:
            print(f"\t- {place.title()}")
    print(f"\n")