#6-7. 
#People:
''' 
Start with the program you wrote for Exercise 6-1 (page 99). 
Make two new dictionaries representing different people, and store all three dictionaries in a list called people. 
Loop through your list of people. As you loop through the list, print everything you know about each person.
'''
'''
6-1
dic={"fname":"rich", "lname":"glazier", "age":50, "city":"denver"}
print(dic)
'''
dict1={"fname":"rich", "lname":"glazier", "age":str(50), "city":"golden"}
dict2={"fname":"randi", "lname":"mauro", "age":str(33), "city":"golden"}
dict3={"fname":"david", "lname":"willson", "age":str(53), "city":"littleton"}
people=[dict1, dict2, dict3]
print("MY PEOPLE")
for l in people:
    print("#"*20)
    for k, v in l.items():
        print(f"{k} .....\t {v.title()}")